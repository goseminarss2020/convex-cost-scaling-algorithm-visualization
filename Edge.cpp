#include "Edge.h"
#include "Edge_MonotonousConst.cpp"

//get state:
Node* Edge::get_a(){
  return a;
}
Node* Edge::get_b(){
  return b;
}
Node* Edge::get_tikz_origin(){
#ifdef DRAW_IMPROVING_EDGES
  return (get_residual_cost(get_a()) < 0 ?  get_a() : get_b());
#else
  return get_a();
#endif
}
Node* Edge::get_tikz_target(){
  return get_tikz_origin() == get_b() ? get_a() : get_b();
}

Edge::MonotonousConst Edge::get_cost_function(){
  return cost;
}

double Edge::get_closest_equilibrium(Node* from){
  double equilibrium = get_cost_function().closest_relative_equilibrium(get_flow(get_a()), get_potential_delta(get_a()));
  if(a == from){
    return equilibrium;
  }else{
    return -equilibrium;
  }
}

double Edge::get_cost(Node* from){
  if(a == from){
    return cost(get_flow(get_a())).inc;
  }else{
    return -cost(get_flow(get_a())).dec;
  }
}

double Edge::get_min(Node* from){
  if(a == from){
    return min;
  }else{
    return -max;
  }
}
double Edge::get_max(Node* from){
  if(a == from){
    return max;
  }else{
    return -min;
  }
}
double Edge::get_flow(Node* from){
  if(a == from){
    return flow;
  }else{
    return -flow;
  }
}

// derived metrics:
double Edge::get_potential_delta(Node* from){
  Node* to = (a == from) ? b : a;
  return  to->get_potential() - from->get_potential();
}

double Edge::get_residual_capacity(Node* from){
  return get_max(from)-get_flow(from);
}

double Edge::get_residual_cost(Node* from){
  return get_cost(from) + get_potential_delta(from);
}

// set state:
void Edge::set_a(Node* a){
  this->a = a;
}
void Edge::set_b(Node* b){
  this->b = b;
}
void Edge::set_min(Node* from, double min){
  if(a == from){
    this->min = min;
  }else{
    this->max = -min;
  }
}
void Edge::set_max(Node* from, double max){
  if(a == from){
    this->max = max;
  }else{
    this->min = -max;
  }
}
void Edge::set_flow(Node* from, double flow){
  double delta = flow - get_flow(from);

  Node* to = (a == from) ? b : a;
  from->set_balance_delta(delta);
  to->set_balance_delta(-delta);

  if(a == from){
    this->flow = flow;
  }else{
    this->flow = -flow;
  }

#ifndef SUPPRESS_PROTOCOL
  std::cout << "Sending " << delta << " flow down \n " << *this << "\n resulting in : " << flow << "\n form " << *from << std::endl;
#endif
}

// refine reset
bool Edge::reset(){
  if(get_cost(get_a()) >= 0 && get_cost(get_b()) >= 0){
    return false;
  }
  double new_flow = get_closest_equilibrium(get_a());
  set_flow(a,new_flow);
  return true;
}


Edge::Edge(Node* a, Node* b, Edge::MonotonousConst cost, double min, double max, double shift_scalar, std::vector<Node::Pos> line_path) : cost(cost), shift_scalar(shift_scalar), line_path(line_path){
  set_a(a);
  set_b(b);
  set_min(a,min);
  set_max(a,max);
  set_flow(a,min);

  a->add_adj(this);
  b->add_adj(this);
}


std::ostream& operator<<(std::ostream& os, Edge& e){
  os << "Edge " << &e << "{\n";

  os << "\t from Node : " << e.get_a() << "\n";
  os << "\t to Node : " << e.get_b() << "\n";
  os << "\t cost : " << e.get_cost(e.get_a()) << "\n";
  os << "\t min : " << e.get_min(e.get_a()) << "\n";
  os << "\t max : " << e.get_max(e.get_a()) << "\n";
  os << "\t flow : " << e.get_flow(e.get_a()) << "\n";
  os << "\t residuum from-to : " << e.get_residual_capacity(e.get_a()) << "\n";
  os << "\t residuum to-from : " << e.get_residual_capacity(e.get_b()) << "\n";
  os << "\t c_p from-to : " << e.get_residual_cost(e.get_a()) << "\n";
  os << "\t c_p to-from : " << e.get_residual_cost(e.get_b()) << "\n";

  os << "}";

  return os;
}
