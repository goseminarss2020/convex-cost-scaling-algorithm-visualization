#ifndef GRAPH_H
#define GRAPH_H


#include "preprocessor_compile_control.h"

#include <vector>
#include <set>
#include <sstream>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <climits>
#include <cmath>
#include <cfloat>

#include "Edge.h"
#include "Node.h"

class Graph{
public:
  struct Changes{
    std::set<Node*> changed_nodes;
    std::set<Edge*> changed_edges;
  };
private:
  std::ofstream tikz_output_stream;
  std::stringstream tikz_picture;
  std::vector<Node*> nodes;
  std::vector<Edge*> edges;

public:
  std::vector<Node*>& get_nodes();
  std::vector<Edge*>& get_edges();

#ifdef CHECK_ASSERT
  void check_all_epsilon_optimality();
#endif
  void update_tikzpicture(Changes);
  void generate_slide(Changes, double);

  Graph(std::vector<Node*>,std::vector<Edge*>, std::string);
  ~Graph();
};

std::ostream& operator<<(std::ostream&, Graph&);

#endif
