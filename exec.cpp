#include "preprocessor_compile_control.h"
#include "Graph.h"
#include "alg.cpp"
#include <climits>
#include <cfloat>

int main(){


  Graph g = Graph(std::vector<Node*>(), std::vector<Edge*>(), INSTANCE);
  std::vector<Node*>& nodes = g.get_nodes();
  std::vector<Edge*>& edges = g.get_edges();

#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mpopulating graph\033[39m"<< std::endl << std::endl;
#endif

  {
    #define DOT(x,y) x.y
    #include TOSTRING(DOT(example_concat_pushes,cpp))
  }

#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mthe graph is given by:\033[39m"<< std::endl << std::endl;
  std::cout << g << std::endl;
  std::cout << "\033[31mstarting cost scaling algorithm\033[39m"<< std::endl << std::endl;
#endif

  cost_scaling(g);

#ifndef SUPPRESS_PROTOCOL
  std::cout << g << std::endl;
#endif
}
