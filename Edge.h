#ifndef EDGE_H
#define EDGE_H


#include "preprocessor_compile_control.h"

#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <set>

#include "Node.h"


class Edge{
public:
  class MonotonousConst{
    std::vector<double> delimiters;
    std::vector<double> values;
  public:
    struct MarginalUtil{
      double dec;
      double inc;
    };

    MonotonousConst(std::vector<double> delimiters, std::vector<double> values);
    double get_abs_max();
    MarginalUtil operator ()(double point);
    double closest_relative_equilibrium(double, double);
  };
private:
  Node* a;
  Node* b;

  MonotonousConst cost;
  double min;
  double max;
  double flow;

public:
  double shift_scalar;
  std::vector<Node::Pos> line_path;
  //get state:

  Node* get_a();
  Node* get_b();
  Node* get_tikz_origin();
  Node* get_tikz_target();

  MonotonousConst get_cost_function();
  double get_closest_equilibrium(Node*);
  double get_cost(Node*);
  double get_min(Node*);
  double get_max(Node*);
  double get_flow(Node*);

  // derived metrics:
  double get_potential_delta(Node*);
  double get_residual_capacity(Node*);
  double get_residual_cost(Node*);

  // set state:
  void set_a(Node*);
  void set_b(Node*);
  void set_min(Node*, double);
  void set_max(Node*, double);
  void set_flow(Node*, double);

  // refine reset
  bool reset();

  Edge(Node*, Node*, MonotonousConst, double, double, double, std::vector<Node::Pos> = {});
};


std::ostream& operator<<(std::ostream&, Edge&);

#endif
