#ifndef PREPROCESSOR_COMPILE_CONTROL_H
/*
 * delete following line if a protocol should be generated
*/
#define SUPPRESS_PROTOCOL







/*
  delete following line if basic assertions should not be checked (for testing)
  leads to asymptotically worse runtime!
*/
//#define CHECK_ASSERT





/*
  delete if cost and residual capacity should not be given on edges of tikzpictures

  PEDANTIC_LABELING is not maintained for convex cost functions
*/
//#define PEDANTIC_LABELING






/*
  delete if tikz edges should always be drawn from get_a() to get_b()
  Otherwise it will be drawn s.t. the edge has non positive get_residual_cost
  Influence limited to get_tikz_origin()'s return value

  improving edges are not uniquely identified in the convex cost case; this is not maintained
*/
//#define DRAW_IMPROVING_EDGES





#define STRINGIFIY(x) #x
#define TOSTRING(x) STRINGIFIY(x)
#define CONCAT(x,y) x ## y
/*
  Variable used to credit on slides
*/
#define ATTRIBUTION "https://git.rwth-aachen.de/goseminarss2020/convex-cost-scaling-algorithm-visualization"

/*
  will execute initialization of INSTANCE##".cpp" (see exec.cpp)
*/

#define INSTANCE TOSTRING(example_concat_pushes)

#endif
