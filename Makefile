CXXFLAGS = -O0 -g -std=c++17 -Wall -Wextra -Wpedantic

a.out: exec.o  Graph.o Edge.o Node.o
	$(CXX) $(CXXFLAGS) -o $@ $^

exec.o: exec.cpp alg.cpp Graph.o example*.cpp
	$(CXX) $(CXXFLAGS) -c $<

Graph.o: Graph.cpp latex.cpp Graph.h Edge.o Node.o preprocessor_compile_control.h
	$(CXX) $(CXXFLAGS) -c $<

Edge.o: Edge.cpp Edge_MonotonousConst.cpp Edge.h Node.o preprocessor_compile_control.h
	$(CXX) $(CXXFLAGS) -c $<

Node.o: Node.cpp Node.h preprocessor_compile_control.h
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f Node.o Edge.o Graph.o exec.o a.out

.PHONY: clean
