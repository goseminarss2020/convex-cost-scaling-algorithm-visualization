{// change the INSTANCE in preprocessor_compile_control.cpp if you want this example to be executed!
  // populate Graph
  // set accordingly before initializing!
  nodes.reserve(3);
  edges.reserve(4);


  nodes.push_back(new Node(2,-1.5));
  nodes.push_back(new Node(0,0));
  nodes.push_back(new Node(4,0));

  edges.push_back(new Edge(nodes[0],nodes[2],Edge::MonotonousConst({0,3},{1}),0,3,-3.5));
  edges.push_back(new Edge(nodes[1],nodes[0],Edge::MonotonousConst({0,1,2,5},{2,3,7}),0,5,-3.5));
  edges.push_back(new Edge(nodes[2],nodes[0],Edge::MonotonousConst({0,1,3},{-1,5}),0,3,-3.5,{{2,0}}));
  // target to source
  edges.push_back(new Edge(nodes[2],nodes[1],Edge::MonotonousConst({1,2},{-9}),1,2,-3.5,{{4,2.5},{-.5,2.5},{-.5,1.5/4}}));
}
