#ifndef NODE_H
#define NODE_H

#include "preprocessor_compile_control.h"

#include <vector>
#include <iostream>
#include <cfloat>

class Edge;

class Node{
public:
  struct Pos{
    double x;
    double y;
    Pos operator-(Pos);
    Pos operator*(double);
  };

private:
  Pos position;
  std::vector<Edge*> adj;
  double potential;
  double balance;

public:
#ifdef CHECK_ASSERT
  double current_epsilon_optimality = DBL_MAX;
  void check_epsilon_optimality();
#endif

  void add_adj(Edge* n);

  Pos get_position();
  std::vector<Edge*> get_adj();
  double get_potential();
  double get_balance();
  void set_potential(double
#ifdef CHECK_ASSERT
    , double
#endif
  );
  void set_balance(double);
  void set_balance_delta(double);

  Node(double, double);
};


std::ostream& operator<<(std::ostream&, Node&);
std::ostream& operator<<(std::ostream&, Node::Pos);

#endif
