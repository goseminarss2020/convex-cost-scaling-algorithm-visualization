#include <cmath>

/*
  Latex creation helper functions
*/

std::string clean_output(double num){
  std::stringstream tmp;
  tmp << std::setprecision(3) << num;
  return (std::abs(num) > INT_MAX/2) ? ((num > 0) ? "\\infty" : "-\\infty") : tmp.str();
}

std::string wrap_minipage(std::string content, char alignment, double width_textwidth, std::string preamble){
  std::stringstream output;
  output << "\\begin{minipage}[" << alignment << "]{" << width_textwidth << "\\textwidth}" << std::endl
          << preamble << std::endl
          << content << std::endl
          << "\\end{minipage}";
  return output.str();
}

double log_argument(Node::Pos p){
  p = {p.x/std::sqrt(p.x*p.x+p.y*p.y), -p.y/std::sqrt(p.x*p.x+p.y*p.y)};
  double s = std::asin(p.y);
  double c = std::acos(p.x);
  if(s == c){
    return s;
  }else{
    return M_PI-s;
  }
}

/*
  Latex creation has been aggregated in this file, functions are associated with ::Graph
*/
/*
  TODO: mark admissable arcs
*/
void Graph::generate_slide(Graph::Changes changes, double eps){
  {
    tikz_output_stream << "\\newpage \n $\\varepsilon = " << eps << "$ \\vspace*{1.5cm} \n" << std::endl;

    tikz_output_stream << wrap_minipage(tikz_picture.str(),'c', .4, "\\flushleft") << std::endl;

    tikz_output_stream << wrap_minipage("\\vspace*{1.2cm}$\\rightsquigarrow$",'c',.15,"\\centering") << "\\hfill" << std::endl;

    tikz_output_stream << "\\vfill {\\tiny \\url{" << ATTRIBUTION << "}}" << std::endl;
  }

  {
    tikz_output_stream << "\\newpage \n $\\varepsilon = " << eps << "$ \\vspace*{1.5cm} \n" << std::endl;

    tikz_output_stream << wrap_minipage(tikz_picture.str(),'c', .4, "\\flushleft") << std::endl;

    tikz_output_stream << wrap_minipage("\\vspace*{1.2cm}$\\rightsquigarrow$",'c',.15,"\\centering") << "\\hfill" << std::endl;

    tikz_picture.str("");
    update_tikzpicture(changes);
    tikz_output_stream << wrap_minipage(tikz_picture.str(), 'c', .4, "\\flushright") << std::endl;

    tikz_output_stream << "\\vfill {\\tiny \\url{" << ATTRIBUTION << "}}" << std::endl;
  }
}


void Graph::update_tikzpicture(Graph::Changes changes){
  tikz_picture << "\\begin{tikzpicture}" << std::endl;
  // Nodes:
  for(Node* n : get_nodes()){
    std::stringstream name("");
    name << n;
    std::string name_str = name.str().substr(name.str().size()-5);
    std::stringstream styles("");
    if(n->get_balance() > 0){
      styles << "active, ";
    }
    if(changes.changed_nodes.find(n) != changes.changed_nodes.end()){
      styles << "update, ";
      if(n->get_balance() > 0){
        styles << "update_active, ";
      }
    }

    tikz_picture << "\\node[vertex, " << styles.str() << "] at " << n->get_position() << "(" << n << ")" << "{\\color{red}$" << clean_output(n->get_potential()) << "$ \\nodepart{lower} \\color{blue}$" << clean_output(n->get_balance()) << "$};" << std::endl;
  }

  // Edges:
  for(Edge* e : get_edges()){

    // drawn edge
    std::stringstream style ("");
    style << "edge, ";
    if(e->get_residual_cost(e->get_tikz_origin()) == 0){
      style << "-, ";
    }else if(e->get_tikz_origin() == e->get_a()){
      style << "->, ";
    }else{
      style << "<-, ";
    }
    if(changes.changed_edges.find(e) != changes.changed_edges.end()){
      style << "edge_update, ";
    }
    if((e->get_residual_cost(e->get_tikz_origin()) < 0
#ifndef DRAW_IMPROVING_EDGES
          || e->get_residual_cost(e->get_tikz_target()) < 0
#endif
        )&& e->get_residual_capacity(e->get_tikz_origin()) > 0){
      style << "admissable, ";
    }

    tikz_picture << "\\draw[" << style.str() << "] (" << e->get_a() << ")";
    for(Node::Pos p : e->line_path){
      tikz_picture << " -- " << p;
    }
    tikz_picture << " -- (" << e->get_b() << ");";

    // guiding edge for node positioning
    Node::Pos direction = {0,0};
    Node::Pos last_pos = e->get_a()->get_position();
    tikz_picture << "\\draw[positioning_edge] (" << e->get_a() << ")";
    if(e->line_path.size() > 0){
      for(size_t i = 0; i < e->line_path.size()/2+1; i++){
        Node::Pos p = e->line_path[i];
        tikz_picture << " -- " << p;
        direction = (p - last_pos);
        last_pos = p;
      }
    }else{
      tikz_picture << " -- (" << e->get_b() << ")";
      direction = (e->get_b()->get_position()-last_pos);
    }
    int precision = tikz_picture.precision();
    tikz_picture.precision(10);
  #ifdef PEDANTIC_LABELING
    Node::Pos shift = direction*e->shift_scalar;
    tikz_picture << "node[pos = .35, xshift=" << -shift.y << ", yshift=" << shift.x << ", edge_label, text width = .75cm]{"
                        << "$c = " << clean_output(e->get_cost(e->get_tikz_origin())) << "$}" << std::endl;

    tikz_picture << "node[pos = .2, xshift=" << -shift.y << ", yshift=" << shift.x << ", edge_label, text width = .75cm]{"
                        << "$c_{p} = " << clean_output(e->get_residual_cost(e->get_tikz_origin())) << "$}" << std::endl;

    tikz_picture << "node[pos = .8, xshift=" << -shift.y << ", yshift=" << shift.x << ", edge_label, text width = 1cm]{"
                        << "$[" << clean_output(e->get_min(e->get_tikz_origin())) << ", " << clean_output(e->get_max(e->get_tikz_origin())) << "]$}" << std::endl;

    tikz_picture << "node[pos = .65, xshift=" << -shift.y << ", yshift=" << shift.x << ", edge_label, text width = 1cm]{"
                        << "$r_f = " << clean_output(e->get_residual_capacity(e->get_tikz_origin())) << "$}" << std::endl;
M_PI
    tikz_picture << "node[pos = .5, midway, xshift=" << -shift.y << ", yshift=" << shift.x << ", edge_label]{"
                        << "$f = " << clean_output(e->get_flow(e->get_tikz_origin())) << "$};" << std::endl;

#else
    tikz_picture << "node[pos = .5, rotate=" << 180*log_argument(direction)/M_PI+180 << ", yshift=" << e->shift_scalar << ", edge_label]{"
                        << "$-c^-_{p}\\!\\! =\\! " << clean_output(e->get_residual_cost(e->get_tikz_target())) << ", c^+_{p}\\!\\! =\\! " << clean_output(e->get_residual_cost(e->get_tikz_origin())) << "$}" << std::endl;

    tikz_picture << "node[pos = .5, rotate=" << 180*log_argument(direction)/M_PI+180 << ", yshift=" << -e->shift_scalar << ", edge_label]{"
                        << "$f\\! = \\!" << clean_output(e->get_flow(e->get_tikz_origin())) << "\\!\\in \\![" << clean_output(e->get_min(e->get_tikz_origin())) << ", " << clean_output(e->get_max(e->get_tikz_origin())) << "]$};" << std::endl;
#endif
    tikz_picture.precision(precision);
  }

  tikz_picture << "\\end{tikzpicture}" << std::endl;
}

void latex_preamble(std::ostream& tikz_os){
  tikz_os << "\\documentclass{beamer}"
                      << "\n\\usepackage{lmodern}\n"
                      << "\\usepackage{amssymb}\n"
                      << "\\usepackage{tikz}\n"
                      << "\\usetikzlibrary{calc, positioning, shapes, shapes.multipart, shadows, arrows}\n"
                      << "\\setbeamersize{text margin left=5pt,text margin right=5pt} \n"
                      << "\\begin{document}\n"
                      << "\\tikzstyle{vertex}=[draw, circle split, draw=gray, fill=white, font=\\fontsize{5}{0}\\selectfont, inner sep = 1pt, line width = 1pt, minimum size=.75cm]\n"
                      << "\\tikzstyle{active}=[draw=teal, double = white,line width = .7pt]\n"
                      << "\\tikzstyle{update_active}=[]\n"
                      << "\\tikzstyle{update}=[double=magenta!50,font=\\fontsize{6}{0}\\selectfont, line width = .7pt]\n"
                      << "\\tikzstyle{vertex_lower_label}=[draw=white, fill = white, text=blue]\n"
                      << "\\tikzstyle{positioning_edge} = [draw = none]\n"
                      << "\\tikzstyle{edge} = [draw = lightgray, line width=1pt, double = white, line width = .3pt]\n"
                      << "\\tikzstyle{edge_update} = [double = magenta!25,line width = .4pt]\n"
                      << "\\tikzstyle{edge_label}=[font=\\fontsize{4}{0}\\selectfont, text centered]\n"
                      << "\\tikzstyle{admissable}=[color = teal]\n"
                      << std::endl;
}
