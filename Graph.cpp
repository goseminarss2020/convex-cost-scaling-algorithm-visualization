#include "Graph.h"
#include "latex.cpp"

std::vector<Node*>& Graph::get_nodes(){
  return nodes;
}
std::vector<Edge*>& Graph::get_edges(){
  return edges;
}

#ifdef CHECK_ASSERT
void Graph::check_all_epsilon_optimality(){
  for(Node* n : get_nodes()){
    n->check_epsilon_optimality();
  }
}
#endif

Graph::Graph(std::vector<Node*> nodes,std::vector<Edge*> edges, std::string name) : nodes(nodes), edges(edges){
  std::filesystem::path dir("./latex");

  if(!(std::filesystem::exists(dir))){
      std::cout<<"Doesn't Exists"<<std::endl;

      if (std::filesystem::create_directory(dir))
          std::cout << "....Successfully Created !" << std::endl;
  }

  std::stringstream s;
  s << "./latex/" << name << ".tex";

  tikz_output_stream = std::ofstream(s.str());
  tikz_output_stream.precision(2);
  tikz_picture = std::stringstream("");
  tikz_picture.precision(2);

  latex_preamble(tikz_output_stream);
}

Graph::~Graph(){
  for(Node* n : get_nodes()){
    delete n;
  }
  for(Edge* e : get_edges()){
    delete e;
  }
  tikz_output_stream << "\\end{document}" << std::endl;
}

std::ostream& operator<<(std::ostream& os, Graph& g){
  os << "\t Nodes: \n";
  for(Node* n : g.get_nodes()){
    os << *n << "\n";
  }
  os << "\t Edges: \n";

  for(Edge* e : g.get_edges()){
    os << *e << "\n";
  }
  os << std::endl;
  return os;
}
