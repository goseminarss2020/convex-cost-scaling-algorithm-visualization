#include <math.h>
#include <algorithm>

Edge::MonotonousConst::MonotonousConst(std::vector<double> delimiters, std::vector<double> values) : delimiters(delimiters), values(values){
  assert(delimiters.size() - 1 == values.size() );
  for(size_t i = 0; i < values.size()-1; i++){
    assert(delimiters[i] < delimiters[i+1]);
    assert(values[i] < values[i+1]);
  }
  assert(delimiters[delimiters.size()-2] < delimiters[delimiters.size()-1]);
}

double Edge::MonotonousConst::get_abs_max(){
  return std::max(std::abs(values[0]),std::abs(values[values.size()-1]));
}


Edge::MonotonousConst::MarginalUtil Edge::MonotonousConst::operator ()(double point){
  if(point <= delimiters[0]){        // clause assumes current pseudo-flow as being cost-finite
    return {-INFINITY, values[0]};
  }else if(delimiters[delimiters.size()-1] <= point){
    return {values[values.size()-1], INFINITY};
  }
  auto closest_lower_bound = std::lower_bound(delimiters.begin(), delimiters.end(), point);
  size_t index = closest_lower_bound - delimiters.begin();
  if(delimiters[index] == point){
    return {values[index-1], values[index]};
  }
  return {values[index-1], values[index-1]};
}


double Edge::MonotonousConst::closest_relative_equilibrium(double flow, double skew){
  /*
    Calculate minimal distant equlibrium point (point where MarginalUtil.dec/.inc > 0) after applying certain potential skew (in application we want get_residual_cost(get_a()),get_residual_cost(get_b()) to be positive)
  */
  auto closest_lower_bound = std::lower_bound(values.begin(), values.end(), -skew);
  size_t index = closest_lower_bound - values.begin();

  if(index < values.size() && values[index] == -skew && delimiters[index] < flow){
    return delimiters[index+1];
  }
  return delimiters[index];
}
