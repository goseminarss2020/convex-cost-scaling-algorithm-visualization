#include "Graph.h"


void refine(Graph& g, double eps){
#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mstarting the pseudoflow initialization\033[39m" << std::endl;
#endif
  std::set<Edge*> changed_edges;
  for(Edge* e : g.get_edges()){
    if(e->reset()){
      changed_edges.insert(e);
    }
  }
  g.generate_slide({{},changed_edges},eps);

#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mstarting the circulation phase\033[39m" << std::endl;
#endif

  while(true){
    bool terminate = true;
    for(Node* n : g.get_nodes()){ // execute pushes
    repeat_iteration:
      if(n->get_balance() > 0){ // node is active
        terminate = false;


        for(Edge* e : n->get_adj()){
          double curr_res_cost = e->get_residual_cost(n);
          double curr_res_cap = e->get_residual_capacity(n);


          if(curr_res_cap > 0){ // pushing possible
            if(curr_res_cost < 0){ // and pushing improves
              double new_flow = std::min(e->get_flow(n)+n->get_balance(),e->get_closest_equilibrium(n));

              e->set_flow(n,new_flow);
              g.generate_slide({{}, {e}},eps);

              if(n->get_balance() == 0){goto repeat_iteration;} // next node will be chosen
            }
          }
        }

        if(n->get_balance() > 0){
          n->set_potential(n->get_potential()+eps
#ifdef CHECK_ASSERT
            , eps
#endif
          );
#ifdef CHECK_ASSERT
          g.check_all_epsilon_optimality();
#endif
          g.generate_slide({{n},{}},eps);

          goto repeat_iteration; //potential increased; new edges admissible
        }
      }
    }

    if(terminate){
#ifndef SUPPRESS_PROTOCOL
      std::cout << g << "\n" << std::endl;
#endif
      break;
    }
  }
}

void cost_scaling(Graph& g){
#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mgenerating slide 0\033[39m" << std::endl << std::endl;
#endif
  g.update_tikzpicture({{},{}});
  double terminate = static_cast<double>(1)/g.get_nodes().size();

  double eps;
#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31mdetermining upper bound on edge cost\033[39m" << std::endl << std::endl;
#endif
  {
    int max = INT_MIN;
    for(Edge* e : g.get_edges()){
      int tmp = e->get_cost_function().get_abs_max();
      if(tmp > max){
        max = tmp;
      }
    }
    eps = max;
  }

#ifndef SUPPRESS_PROTOCOL
  std::cout << "\033[31meps initialized with \033[39m" << eps << std::endl << std::endl;
#endif

  while(eps > terminate){
    eps = eps/2;
#ifndef SUPPRESS_PROTOCOL
    std::cout << "\033[31mstarting refine with \033[39m"<< "\\eps = " << eps <<  std::endl << std::endl;
#endif
    refine(g,eps);
  }
}
