#include "Edge.h"

Node::Pos Node::Pos::operator-(Pos a){
  return {this->x-a.x, this->y-a.y};
}

Node::Pos Node::Pos::operator*(double r){
  return {this->x*r, this->y*r};
}

#ifdef CHECK_ASSERT
void Node::check_epsilon_optimality(){
  for(Edge* e : get_adj()){
    if(e->get_residual_cost(this) < -current_epsilon_optimality && e->get_residual_capacity(this) > 0){
      std::cerr << "\033[1;31mbold Local epsilon optimality not non-decreasing! \033[0m\n" << std::endl;
    }
  }
}
#endif

void Node::add_adj(Edge* n){
  adj.push_back(n);
}


Node::Pos Node::get_position(){
  return position;
}

std::vector<Edge*> Node::get_adj(){
  return adj;
}
double Node::get_potential(){
  return potential;
}
double Node::get_balance(){
  return balance;
}

void Node::set_potential(double potential
#ifdef CHECK_ASSERT
  , double eps
#endif
){
#ifdef CHECK_ASSERT
  if(potential <= this->potential){
    std::cerr << "\033[1;31mbold Potential decreased! \033[0m\n" << std::endl;
  }
#endif
#ifndef SUPPRESS_PROTOCOL
  std::cout << "Changing Potential of " << *this
            << "from " << this->potential << " to " << potential << " difference " << potential - this->potential << std::endl;
#endif
  this->potential = potential;
#ifdef CHECK_ASSERT
  this->current_epsilon_optimality = eps;
  check_epsilon_optimality();
#endif
}

void Node::set_balance(double balance){
#ifndef SUPPRESS_PROTOCOL
  std::cout << "Changing Balance of " << *this
            << "from " << this->balance << " to " << balance << " difference " << balance - this->balance << std::endl;
#endif
  this->balance = balance;
}
void Node::set_balance_delta(double bal_delta){
  set_balance(get_balance() - bal_delta);
}

Node::Node(double x, double y) : position({x,y}), adj(std::vector<Edge*>()), potential(0), balance(0) {}

std::ostream& operator<<(std::ostream& os, Node& n){
  os << "Node " << &n << "{\n";

  for(Edge* ep : n.get_adj()){
    os << "\t\t " << ep << "\n";
  }

  os << "\t potential : " << n.get_potential() << "\n";
  os << "\t balance : " << n.get_balance() << "\n";

  os << "}";

  return os;
}

std::ostream& operator<<(std::ostream& os, Node::Pos position){
  os << "(" << position.x << ", " << position.y << ")";
  return os;
}
