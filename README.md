Purpose
===
Generate latex slides visualizing the operations of the algorithm described in **[2]** on arbitrary graphs. This is the convex variant. For the analogous algorithm described in **[1]** operating on graphs with linear costs / constant marginal costs see [here](https://git.rwth-aachen.de/jonasseidel/linear-cost-scaling-algorithm-visualization) (this is a fork of said project).

## Disclaimer
This project does not strive to be an efficient implementation. Since it is a fork of the linear variant it uses none of the complexity improving properties of our problem. Slides are generated, that faithfully recreate the algorithms idea. No warranty for anything :).

Use
===
populate Graph like already done in main, execute. Output will be written to ./latex/;  folder created in Graph::Graph. Tested on Pop!\_OS 20.04 LTS; your milage may vary.


---
*  **[1]** Andrew V. Goldberg, Robert E. Tarjan _Solving Minimum-Cost Flow Problems by Successive Approximation_ Proceedings of the nineteenth annual ACM symposium on Theory of computing (1987)
* **[2]** Ravindra K. Ahuja, Dorit S. Hochbaum, James B. Orlin _Solving the Convex Cost Integer Dual Network Flow Problem_ Management Science (2003)
